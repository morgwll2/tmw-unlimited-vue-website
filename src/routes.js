import Section from './components/Section.vue';

export const routes = [
    { path: '', component: Section },
    { path: '/work-and-clients', component: Section },
    { path: '/approach-and-services', component: Section },
    { path: '/latest-news', component: Section },
    { path: '/people', component: Section },
    { path: '/careers', component: Section }
]